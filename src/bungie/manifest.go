package bungie

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/redis/rueidis"
)

type ManifestInfoResp struct {
	Version                  string
	MobileAssetContentPath   string
	MobileGearAssetDataBases []struct {
		Version int
		Path    string
	}
	MobileWorldContentPaths        map[string]string
	JsonWorldContentPaths          map[string]string
	JsonWorldComponentContentPaths map[string]map[string]string
	MobileClanBannerDatabasPath    string
	MobileGearCDN                  struct {
		Geometry    string
		Texture     string
		PlateRegion string
		Gear        string
		Shader      string
	}
}

const (
	TableDestinyDamageTypeDefinition    = "DestinyDamageTypeDefinition"
	TableDestinyStatDefinition          = "DestinyStatDefinition"
	TableDestinyVendorDefinition        = "DestinyVendorDefinition"
	TableDestinyInventoryItemDefinition = "DestinyInventoryItemDefinition"
)

type DestinyVendorDefinition struct {
	ItemList []struct {
		ItemHash int
	}
	Locations []struct {
		DestinationHash int
	}
}

type DestinyInventoryItemDefinition struct {
	ClassType int
	Hash      int
}

var lastRefreshes map[string]int64 = make(map[string]int64)

func (b *BungieAPIClient) refreshManifest(table string) bool {
	ctx := context.Background()

	var cachedVersionResp []string
	var cachedVersion string
	err := b.rdb.Do(ctx, b.rdb.B().JsonGet().Key("manifest:versions").Path("$."+table).Build()).DecodeJSON(&cachedVersionResp)
	if err != nil {
		if rueidis.IsRedisNil(err) {
			err = b.rdb.Do(ctx, b.rdb.B().JsonSet().Key("manifest:versions").Path("$").Value("{}").Build()).Error()
			if err != nil {
				panic(err)
			}
			cachedVersion = "notaversion"
		} else {
			fmt.Println(err)
			return false
		}
	} else {
		if len(cachedVersionResp) == 0 {
			cachedVersion = "notaversion"
		} else {
			cachedVersion = cachedVersionResp[0]
		}
	}

	req, err := http.NewRequest(http.MethodGet, "https://www.bungie.net/Platform/Destiny2/Manifest/", nil)
	if err != nil {
		fmt.Println(err)
		return false
	}

	apiKey := os.Getenv("BUNGIE_API_KEY")
	if apiKey == "" {
		panic("BUNGIE_API_KEY not set")
	}
	req.Header.Add("X-API-Key", apiKey)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return false
	}

	defer resp.Body.Close()
	jsonDump, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(resp.StatusCode)
		panic(err)
	}

	if resp.Header.Get("Content-type") != "application/json; charset=utf-8" {
		fmt.Println(string(jsonDump))
		fmt.Println(resp.Header.Get("Content-type"))
		fmt.Println(resp.StatusCode)
		panic("Non-JSON response")
	}

	manifests := BungieResp[ManifestInfoResp]{}

	err = json.Unmarshal(jsonDump, &manifests)
	if err != nil {
		fmt.Println(err)
		fmt.Println(string(jsonDump[:]))
		fmt.Println(resp.StatusCode)
		return false
	}

	if cachedVersion != manifests.Response.Version {
		fmt.Println("Pulling new manifest")
		resp, err := http.Get("https://www.bungie.net" + manifests.Response.JsonWorldComponentContentPaths["en"][table])
		if err != nil {
			fmt.Println(resp.Request.URL)
			panic(err)
		}

		defer resp.Body.Close()
		jsonDump, err := io.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}

		err = b.rdb.Do(ctx, b.rdb.B().JsonSet().Key("manifest:"+table).Path("$").Value(string(jsonDump[:])).Build()).Error()
		if err != nil {
			panic(err)
		}
		err = b.rdb.Do(ctx, b.rdb.B().JsonSet().Key("manifest:versions").Path("$."+table).Value("\""+manifests.Response.Version+"\"").Build()).Error()
		if err != nil {
			panic(err)
		}
	}
	return true
}

func (b *BungieAPIClient) GetItemFromManifest(table string, id string) string {
	currentTime := time.Now().Unix()
	lastRefresh, ok := lastRefreshes[table]
	if !ok || currentTime > lastRefresh+5 {
		fmt.Println("Checking table " + table)
		success := b.refreshManifest(table)
		if !success {
			fmt.Println("API down, using cached manifest")
		}
		lastRefreshes[table] = currentTime
	}

	item, err := b.rdb.Do(context.Background(), b.rdb.B().JsonGet().Key("manifest:"+table).Path("."+id).Build()).ToString()
	if err != nil {
		panic(err)
	}

	return item
}

func (b *BungieAPIClient) GetVendorDef(hash string) DestinyVendorDefinition {
	def := b.GetItemFromManifest(TableDestinyVendorDefinition, hash)
	var vendorDef DestinyVendorDefinition
	err := json.Unmarshal([]byte(def), &vendorDef)
	if err != nil {
		panic(err)
	}
	return vendorDef
}

func (b *BungieAPIClient) GetItemDefFromVendorDef(hash string, itemIndex int) DestinyInventoryItemDefinition {
	vendorDef := b.GetVendorDef(hash)
	itemHash := vendorDef.ItemList[itemIndex].ItemHash
	def := b.GetItemFromManifest(TableDestinyInventoryItemDefinition, fmt.Sprint(itemHash))
	var itemDef DestinyInventoryItemDefinition
	err := json.Unmarshal([]byte(def), &itemDef)
	if err != nil {
		panic(err)
	}
	return itemDef
}

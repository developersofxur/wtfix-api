package bungie

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/redis/rueidis"
)

const (
	ClassTitan   = 0
	ClassHunter  = 1
	ClassWarlock = 2
	ClassShared  = 3
)

const (
	ComponentItemInstances     = "300"
	ComponentItemStats         = "304"
	ComponentItemSockets       = "305"
	ComponentItemReusablePlugs = "310"
	ComponentVendors           = "400"
	ComponentVendorCategories  = "401"
	ComponentVendorSales       = "402"
)

type BungieTokenResp struct {
	Access_Token       string
	Token_Type         string
	Expires_In         int
	Refresh_Token      string
	Refresh_Expires_In int
	Membership_Id      string
}

type BungieResp[T any] struct {
	Response        T
	ErrorCode       int
	ThrottleSeconds int
	ErrorStatus     string
	Message         string
	MessageData     map[string]any
}

type BungieAPIClient struct {
	authToken        string
	authTokenExpires time.Time
	rdb              rueidis.Client

	refreshInfo *BungieRefreshInfo
}

type BungieRefreshInfo struct {
	Token   string    `json:"token"`
	Expires time.Time `json:"expires"`
}

func NewBungieAPIClient(rdb rueidis.Client) *BungieAPIClient {
	return &BungieAPIClient{
		authToken:        "",
		authTokenExpires: time.Time{},
		rdb:              rdb,
		refreshInfo:      nil,
	}
}

func (b *BungieAPIClient) Login(code string) {
	b.tokenQuery(false, code)
}

func (b *BungieAPIClient) tokenQuery(isRefresh bool, code string) {
	clientId := os.Getenv("BUNGIE_API_ID")
	if clientId == "" {
		panic("BUNGIE_API_ID not set")
	}

	clientSecret := os.Getenv("BUNGIE_API_SECRET")
	if clientSecret == "" {
		panic("BUNGIE_API_SECRET not set")
	}

	codeKey := "code"
	grantType := "authorization_code"
	if isRefresh {
		codeKey = "refresh_token"
		grantType = "refresh_token"
	}

	params := url.Values{}
	params.Add("grant_type", grantType)
	params.Add(codeKey, code)
	params.Add("client_id", clientId)
	params.Add("client_secret", clientSecret)
	body := params.Encode()

	req, err := http.NewRequest(http.MethodPost, "https://www.bungie.net/platform/app/oauth/token/", strings.NewReader(body))
	if err != nil {
		panic(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := http.Client{}
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	jsonDump, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(res.StatusCode)
		panic(err)
	}

	if res.StatusCode != 200 {
		fmt.Println(string(jsonDump))
		panic("Non-200 response")
	}

	var tokenResp BungieTokenResp
	err = json.Unmarshal(jsonDump, &tokenResp)
	if err != nil {
		panic(err)
	}

	b.authToken = tokenResp.Access_Token
	b.authTokenExpires = time.Now().Add(time.Duration(tokenResp.Expires_In) * time.Second)
	newRefreshInfo := BungieRefreshInfo{
		Token:   tokenResp.Refresh_Token,
		Expires: time.Now().Add(time.Duration(tokenResp.Refresh_Expires_In) * time.Second),
	}
	b.refreshInfo = &newRefreshInfo

	refreshString, err := json.Marshal(newRefreshInfo)
	if err != nil {
		panic(err)
	}

	err = b.rdb.Do(context.Background(), b.rdb.B().Set().Key("server:refresh").Value(string(refreshString)).Build()).Error()
	if err != nil {
		panic(err)
	}
}

func (b *BungieAPIClient) fetchWithHeaders(path string, queryParams map[string]any, headers map[string]string) []byte {
	apiKey := os.Getenv("BUNGIE_API_KEY")
	if apiKey == "" {
		panic("BUNGIE_API_KEY not set")
	}

	req, err := http.NewRequest(http.MethodGet, "https://www.bungie.net/Platform"+path, nil)
	if err != nil {
		panic(err)
	}

	params := url.Values{}
	for k, v := range queryParams {
		params.Add(k, v.(string))
	}
	req.URL.RawQuery = params.Encode()

	for k, v := range headers {
		req.Header.Add(k, v)
	}
	req.Header.Add("X-API-Key", apiKey)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	defer res.Body.Close()
	jsonDump, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	if res.Header.Get("Content-type") != "application/json; charset=utf-8" {
		fmt.Println(string(jsonDump))
		fmt.Println(res.Header.Get("Content-type"))
		fmt.Println(res.StatusCode)
		panic("Non-JSON response")
	}

	// if res.StatusCode != 200 {
	// 	fmt.Println(string(jsonDump))
	// 	panic("Non-200 response")
	// }

	return jsonDump
}

func (b *BungieAPIClient) fetch(path string, queryParams map[string]any) []byte {
	return b.fetchWithHeaders(path, queryParams, map[string]string{})
}

func (b *BungieAPIClient) authFetch(path string, queryParams map[string]any) ([]byte, error) {
	if b.refreshInfo == nil {
		refreshString, err := b.rdb.Do(context.Background(), b.rdb.B().Get().Key("server:refresh").Build()).ToString()
		if err != nil {
			if rueidis.IsRedisNil(err) {
				return nil, errors.New("No refresh token, please auth")
			} else {
				panic(err)
			}
		}

		var newRefreshInfo BungieRefreshInfo
		err = json.Unmarshal([]byte(refreshString), &newRefreshInfo)
		if err != nil {
			fmt.Print(err)
			panic(err)
		}

		b.refreshInfo = &newRefreshInfo
	}

	if b.authToken == "" || b.authTokenExpires.IsZero() || time.Now().After(b.authTokenExpires) {
		if time.Now().After(b.refreshInfo.Expires) {
			panic("Refresh token expired, please re-auth")
		}

		b.tokenQuery(true, b.refreshInfo.Token)
	}

	return b.fetchWithHeaders(path, queryParams, map[string]string{
		"Authorization": "Bearer " + b.authToken,
	}), nil
}

func getProfileApiPath() string {
	plat := os.Getenv("BUNGIE_CHAR_PLAT")
	if plat == "" {
		panic("BUNGIE_CHAR_PLAT not set")
	}

	member := os.Getenv("BUNGIE_CHAR_MEMBERID")
	if member == "" {
		panic("BUNGIE_CHAR_MEMBERID not set")
	}

	return fmt.Sprintf("/Destiny2/%s/Profile/%s", plat, member)
}

func getCharId(classType int) string {
	envKey := ""
	switch classType {
	case ClassTitan:
		envKey = "BUNGIE_CHAR_TITANID"
	case ClassHunter:
		envKey = "BUNGIE_CHAR_HUNTERID"
	case ClassWarlock:
		envKey = "BUNGIE_CHAR_WARLOCKID"
	}
	if envKey == "" {
		panic("Invalid class type")
	}

	char := os.Getenv(envKey)
	if char == "" {
		panic(envKey + " not set")
	}

	return char
}

func getCharacterApiPath(classType int) string {
	return getProfileApiPath() + "/Character/" + getCharId(classType)
}

package bungie

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"time"
)

const xurVendorHash = "2190858386"

var mapTypes = []string{"doodle"}

const morningResetHour = 17
const xurDayOfWeek = time.Friday
const weeklyResetDayOfWeek = time.Tuesday

type XurInventory struct {
	Weapons ItemGroups `json:"weapons"`
	Armor   ItemGroups `json:"armor"`
}

type ItemGroups struct {
	Shared  []Item `json:"shared"`
	Warlock []Item `json:"warlock"`
	Hunter  []Item `json:"hunter"`
	Titan   []Item `json:"titan"`
}

type Item struct {
	ItemHash      int            `json:"itemHash"`
	Instance      map[string]any `json:"instance"`
	Stats         map[string]any `json:"stats"`
	Sockets       map[string]any `json:"sockets"`
	ReusablePlugs map[string]any `json:"reusablePlugs"`
}

type XurStatus struct {
	Present    bool         `json:"present"`
	LastUpdate *time.Time   `json:"lastUpdate"`
	Location   *XurLocation `json:"location"`
}

type XurLocation struct {
	Planet    string   `json:"planet"`
	Zone      string   `json:"zone"`
	Desc      string   `json:"desc"`
	CustomMap *string  `json:"customMap"`
	Maps      *XurMaps `json:"maps"`
}

type XurMaps struct {
	Featured string            `json:"featured"`
	Site     map[string]string `json:"site"`
	Bot      map[string]string `json:"bot"`
	White    map[string]string `json:"white"`
}

func (b *BungieAPIClient) GetXurInventory() (*XurInventory, error) {
	defChan := make(chan DestinyVendorDefinition)
	warlockChan := make(chan DestinyVendorResponse)
	warlockErrChan := make(chan error)
	hunterChan := make(chan DestinyVendorResponse)
	hunterErrChan := make(chan error)
	titanChan := make(chan DestinyVendorResponse)
	titanErrChan := make(chan error)
	classOrder := []int{ClassWarlock, ClassHunter, ClassTitan}
	for _, class := range classOrder {
		var classChan chan DestinyVendorResponse
		var errChan chan error
		switch class {
		case ClassWarlock:
			classChan = warlockChan
			errChan = warlockErrChan
		case ClassTitan:
			classChan = titanChan
			errChan = titanErrChan
		case ClassHunter:
			classChan = hunterChan
			errChan = hunterErrChan
		}

		go func(ch chan DestinyVendorResponse, ech chan error, class int) {
			resp, err := b.GetVendorOnClass(xurVendorHash, class)
			if err != nil {
				ech <- err
			}
			ch <- resp
		}(classChan, errChan, class)
	}

	go func(ch chan DestinyVendorDefinition) {
		ch <- b.GetVendorDef(xurVendorHash)
	}(defChan)

	xurDef := <-defChan
	vendorResps := []DestinyVendorResponse{}

	select {
	case err := <-warlockErrChan:
		return nil, err
	case resp := <-warlockChan:
		vendorResps = append(vendorResps, resp)
	}

	select {
	case err := <-hunterErrChan:
		return nil, err
	case resp := <-hunterChan:
		vendorResps = append(vendorResps, resp)
	}

	select {
	case err := <-titanErrChan:
		return nil, err
	case resp := <-titanChan:
		vendorResps = append(vendorResps, resp)
	}

	xurInventory := &XurInventory{
		Weapons: ItemGroups{
			Shared:  []Item{},
			Warlock: []Item{},
			Hunter:  []Item{},
			Titan:   []Item{},
		},
		Armor: ItemGroups{
			Shared:  []Item{},
			Warlock: []Item{},
			Hunter:  []Item{},
			Titan:   []Item{},
		},
	}

	xurInventory.Weapons.Shared = append(xurInventory.Weapons.Shared, buildItemInfo(xurDef, vendorResps[0], vendorResps[0].Categories.Data.Categories[0].ItemIndexes[1]))
	xurInventory.Weapons.Shared = append(xurInventory.Weapons.Shared, buildItemInfo(xurDef, vendorResps[0], vendorResps[0].Categories.Data.Categories[1].ItemIndexes[0]))

	sharedWepIndexes := []int{}
	for i, vendorResp := range vendorResps {
		for _, weaponIndex := range vendorResp.Categories.Data.Categories[2].ItemIndexes {
			weaponDef := b.GetItemDefFromVendorDef(xurVendorHash, weaponIndex)
			weaponItem := buildItemInfo(xurDef, vendorResp, weaponIndex)
			switch weaponDef.ClassType {
			case ClassTitan:
				xurInventory.Weapons.Titan = append(xurInventory.Weapons.Titan, weaponItem)
			case ClassHunter:
				xurInventory.Weapons.Hunter = append(xurInventory.Weapons.Hunter, weaponItem)
			case ClassWarlock:
				xurInventory.Weapons.Warlock = append(xurInventory.Weapons.Warlock, weaponItem)
			case ClassShared:
				if !contains(sharedWepIndexes, weaponIndex) {
					xurInventory.Weapons.Shared = append(xurInventory.Weapons.Shared, weaponItem)
					sharedWepIndexes = append(sharedWepIndexes, weaponIndex)
				}
			}
		}

		exoticPosition := -1
		switch classOrder[i] {
		case ClassTitan:
			exoticPosition = 3
		case ClassHunter:
			exoticPosition = 2
		case ClassWarlock:
			exoticPosition = 4
		}
		exoticArmorIndex := vendorResp.Categories.Data.Categories[0].ItemIndexes[exoticPosition]
		exoticArmorItem := buildItemInfo(xurDef, vendorResp, exoticArmorIndex)
		switch classOrder[i] {
		case ClassTitan:
			xurInventory.Armor.Titan = append(xurInventory.Armor.Titan, exoticArmorItem)
		case ClassHunter:
			xurInventory.Armor.Hunter = append(xurInventory.Armor.Hunter, exoticArmorItem)
		case ClassWarlock:
			xurInventory.Armor.Warlock = append(xurInventory.Armor.Warlock, exoticArmorItem)
		}

		for _, armorIndex := range vendorResp.Categories.Data.Categories[3].ItemIndexes {
			armorItem := buildItemInfo(xurDef, vendorResp, armorIndex)
			switch classOrder[i] {
			case ClassTitan:
				xurInventory.Armor.Titan = append(xurInventory.Armor.Titan, armorItem)
			case ClassHunter:
				xurInventory.Armor.Hunter = append(xurInventory.Armor.Hunter, armorItem)
			case ClassWarlock:
				xurInventory.Armor.Warlock = append(xurInventory.Armor.Warlock, armorItem)
			}
		}
	}

	return xurInventory, nil
}

func (b *BungieAPIClient) RefreshXurStatus() (bool, error) {
	fmt.Println("Refreshing Xur status")
	newXurStatus := XurStatus{
		Present: false,
	}
	if IsItXurTime() {
		xurResp, err := b.GetVendor(xurVendorHash)
		if err != nil {
			newXurStatus = XurStatus{
				Present:    true,
				LastUpdate: getNowPointer(),
				Location:   nil,
			}
		} else {
			xurDef := b.GetVendorDef(xurVendorHash)
			xurLocation := getXurLocation(xurDef.Locations[xurResp.Vendor.Data.VendorLocationIndex].DestinationHash)
			newXurStatus = XurStatus{
				Present:    true,
				LastUpdate: getNowPointer(),
				Location:   xurLocation,
			}
		}
	}

	exists, err := b.rdb.Do(context.Background(), b.rdb.B().Exists().Key("wtfix:xur").Build()).AsBool()
	if err != nil {
		panic(err)
	}
	if exists {
		var oldXurStatus XurStatus
		err = b.rdb.Do(context.Background(), b.rdb.B().JsonGet().Key("wtfix:xur").Path(".").Build()).DecodeJSON(&oldXurStatus)
		if err != nil {
			panic(err)
		}

		if (oldXurStatus.Present && newXurStatus.Present && (oldXurStatus.Location != nil || newXurStatus.Location == nil)) || (!oldXurStatus.Present && !newXurStatus.Present) {
			return false, nil
		}
	}

	jsonDump, err := json.Marshal(newXurStatus)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(jsonDump))
	err = b.rdb.Do(context.Background(), b.rdb.B().JsonSet().Key("wtfix:xur").Path("$").Value(string(jsonDump)).Build()).Error()
	if err != nil {
		panic(err)
	}
	fmt.Println("Xur status updated")
	err = b.rdb.Do(context.Background(), b.rdb.B().Publish().Channel("xur").Message("").Build()).Error()
	if err != nil {
		panic(err)
	}
	UpdateFrontend()
	return true, nil
}

func (b *BungieAPIClient) GetXurStatus() XurStatus {
	var XurStatus XurStatus
	exists, err := b.rdb.Do(context.Background(), b.rdb.B().Exists().Key("wtfix:xur").Build()).AsBool()
	if err != nil {
		panic(err)
	}
	if !exists {
		_, err = b.RefreshXurStatus()
		if err != nil {
			panic(err)
		}
	}
	err = b.rdb.Do(context.Background(), b.rdb.B().JsonGet().Key("wtfix:xur").Path(".").Build()).DecodeJSON(&XurStatus)
	if err != nil {
		panic(err)
	}

	return XurStatus
}

func UpdateFrontend() {
	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		fmt.Println("API_KEY not set")
	} else {
		params := url.Values{}
		params.Add("key", apiKey)
		paramsString := params.Encode()
		_, err := http.Get("http://server/api/refresh?" + paramsString)
		if err != nil {
			fmt.Println("Error during refresh ", err)
		}
	}
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func buildItemInfo(vendorDef DestinyVendorDefinition, vendorResp DestinyVendorResponse, itemIndex int) Item {
	item := Item{
		ItemHash:      vendorDef.ItemList[itemIndex].ItemHash,
		Instance:      vendorResp.ItemComponents.Instances.Data[itemIndex],
		Stats:         vendorResp.ItemComponents.Stats.Data[itemIndex],
		Sockets:       vendorResp.ItemComponents.Sockets.Data[itemIndex],
		ReusablePlugs: vendorResp.ItemComponents.ReusablePlugs.Data[itemIndex],
	}
	return item
}

func getXurLocation(hash int) *XurLocation {
	featuredMap := mapTypes[rand.Intn(len(mapTypes))]
	switch hash {
	case 1737926756:
		return &XurLocation{
			Planet: "Tower",
			Zone:   "The Hangar",
			Desc:   "Behind Dead Orbit",
			Maps: &XurMaps{
				Featured: featuredMap,
				Site: map[string]string{
					"doodle": "/images/maps/doodle/tower_red.png",
				},
				Bot: map[string]string{
					"doodle": "/images/maps/doodle/tower.png",
				},
				White: map[string]string{
					"doodle": "/images/maps/doodle/tower_white.png",
				},
			},
		}
	case 697502628:
		return &XurLocation{
			Planet: "Earth",
			Zone:   "Winding Cove",
			Desc:   "On his cliff",
			Maps: &XurMaps{
				Featured: featuredMap,
				Site: map[string]string{
					"doodle": "/images/maps/doodle/earth_red.png",
				},
				Bot: map[string]string{
					"doodle": "/images/maps/doodle/earth.png",
				},
				White: map[string]string{
					"doodle": "/images/maps/doodle/earth_white.png",
				},
			},
		}
	case 3607432451:
		return &XurLocation{
			Planet: "Nessus",
			Zone:   "Watcher's Grave",
			Desc:   "In his tree",
			Maps: &XurMaps{
				Featured: featuredMap,
				Site: map[string]string{
					"doodle": "/images/maps/doodle/nessus_red.png",
				},
				Bot: map[string]string{
					"doodle": "/images/maps/doodle/nessus.png",
				},
				White: map[string]string{
					"doodle": "/images/maps/doodle/nessus_white.png",
				},
			},
		}
	}
	return nil
}

func IsItXurTime() bool {
	time := time.Now().UTC()
	weekday := time.Weekday()
	hour := time.Hour()

	return weekday > xurDayOfWeek || weekday < weeklyResetDayOfWeek || (weekday == xurDayOfWeek && hour >= morningResetHour) || (weekday == weeklyResetDayOfWeek && hour < morningResetHour)
}

func getNowPointer() *time.Time {
	now := time.Now().UTC()
	return &now
}

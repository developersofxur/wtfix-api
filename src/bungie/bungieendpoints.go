package bungie

import (
	"encoding/json"
	"strings"
)

type DestinyVendorResponse struct {
	Vendor struct {
		Data struct {
			VendorLocationIndex int
		}
	}
	Categories struct {
		Data struct {
			Categories []struct {
				ItemIndexes []int
			}
		}
	}
	ItemComponents struct {
		Instances struct {
			Data map[int]map[string]any
		}
		Stats struct {
			Data map[int]map[string]any
		}
		Sockets struct {
			Data map[int]map[string]any
		}
		ReusablePlugs struct {
			Data map[int]map[string]any
		}
	}
}

type VendorNotFound struct{}

func (v *VendorNotFound) Error() string {
	return "Vendor not found"
}

func (b *BungieAPIClient) GetVendor(vendorHash string) (DestinyVendorResponse, error) {
	return b.GetVendorOnClass(vendorHash, ClassWarlock)
}

func (b *BungieAPIClient) GetVendorOnClass(vendorHash string, classType int) (DestinyVendorResponse, error) {
	resp, err := b.authFetch(getCharacterApiPath(classType)+"/Vendors/"+vendorHash, map[string]any{"components": strings.Join([]string{ComponentItemInstances, ComponentItemStats, ComponentItemSockets, ComponentItemReusablePlugs, ComponentVendors, ComponentVendorCategories}[:], ",")})
	if err != nil {
		return DestinyVendorResponse{}, err
	}

	var vendorResp BungieResp[DestinyVendorResponse]
	err = json.Unmarshal(resp, &vendorResp)
	if err != nil {
		panic(err)
	}

	if vendorResp.ErrorStatus == "DestinyVendorNotFound" {
		return vendorResp.Response, &VendorNotFound{}
	} else {
		return vendorResp.Response, nil
	}
}

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
	"wtfix/api/src/bungie"

	"github.com/gin-gonic/gin"
	"github.com/mitchellh/hashstructure/v2"
	"github.com/redis/rueidis"
)

var bungieApi *bungie.BungieAPIClient

func getItemFromManifest(c *gin.Context) {
	id := c.Param("id")
	table := c.Param("table")

	item := bungieApi.GetItemFromManifest(table, id)

	c.Data(http.StatusOK, "application/json", []byte(item))
}

func getXurInventory(c *gin.Context) {
	inv, err := bungieApi.GetXurInventory()
	if err != nil {
		if errors.Is(err, &bungie.VendorNotFound{}) {
			c.Data(http.StatusOK, "application/json", []byte("{}"))
			return
		}
		panic(err)
	}

	jsonDump, err := json.Marshal(inv)
	if err != nil {
		panic(err)
	}

	c.Data(http.StatusOK, "application/json", jsonDump)
}

func getXurStatus(c *gin.Context) {
	xurStatus := bungieApi.GetXurStatus()

	jsonDump, err := json.Marshal(xurStatus)
	if err != nil {
		panic(err)
	}

	c.Data(http.StatusOK, "application/json", jsonDump)
}

func redirect(c *gin.Context) {
	code := c.Query("code")
	bungieApi.Login(code)

	c.String(http.StatusOK, "ok")
}

func healthz(c *gin.Context) {
	c.String(http.StatusOK, "ok")
}

func refreshLoop() {
	prevXurTime := bungie.IsItXurTime()
	prevInventoryHash := uint64(0)
	quickRefresh := false
	for {
		if !prevXurTime && bungie.IsItXurTime() {
			quickRefresh = true
			prevInventoryHash = 0
		}
		updated, err := bungieApi.RefreshXurStatus()
		if err != nil {
			fmt.Println("Error during xur refresh ", err)
		} else {
			quickRefresh = false

			if bungie.IsItXurTime() {
				newInventory, err := bungieApi.GetXurInventory()
				if err != nil {
					fmt.Println("Error during xur inventory refresh ", err)
				} else {
					newInventoryHash, err := hashstructure.Hash(*newInventory, hashstructure.FormatV2, nil)
					if err != nil {
						fmt.Println("Error during xur inventory hash ", err)
					} else {
						if !updated && prevInventoryHash != newInventoryHash {
							fmt.Println("Revalidating frontend")
							bungie.UpdateFrontend()
						}

						prevInventoryHash = newInventoryHash
					}
				}
			}
		}

		duration := 60
		if quickRefresh {
			duration = 5
		}
		time.Sleep(time.Duration(duration) * time.Second)
	}
}

func main() {
	var err error
	rdb, err := rueidis.NewClient(rueidis.ClientOption{InitAddress: []string{"redis:6379"}})
	if err != nil {
		panic(err)
	}

	bungieApi = bungie.NewBungieAPIClient(rdb)

	_, err = bungieApi.RefreshXurStatus()
	if err != nil {
		fmt.Println("Error during initial xur refresh", err)
	}

	go refreshLoop()

	router := gin.New()
	router.Use(
		gin.LoggerWithWriter(gin.DefaultWriter, "/healthz"),
		gin.Recovery(),
	)

	router.GET("/manifest/:table/:id", getItemFromManifest)
	router.GET("/xur/inventory", getXurInventory)
	router.GET("/xur/status", getXurStatus)
	router.GET("/redirect", redirect)
	router.GET("/healthz", healthz)

	fmt.Println("Starting server")
	router.Run("0.0.0.0:80")
}

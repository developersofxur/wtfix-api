FROM golang:1.21 as builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY src ./src
RUN CGO_ENABLED=0 GOOS=linux go build ./src/...
RUN CGO_ENABLED=0 GOOS=linux go build -o /wtfix-api ./src/main.go



FROM alpine:latest

COPY --from=builder /wtfix-api /wtfix-api

CMD ["/wtfix-api"]
